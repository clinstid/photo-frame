const compression = require('compression');
const express = require('express');
const mustache = require('mustache');
const winston = require('winston');
const fs = require('fs');
var argv = require('minimist')(process.argv.slice(2), {
    default: {
        port: 7000,
        key: null,
        ddRumClientToken: null,
        ddRumAppId: null,
        logFile: "/var/log/photoframe/photoframe.log"
    }
});

const consoleTransport = new winston.transports.Console();
const fileTransport = new winston.transports.File({filename: argv.logFile});
const log = new winston.createLogger({transports: [consoleTransport, fileTransport]});

const app = express();
app.use(compression());

function logRequest(req, res, next) {
    log.info(req.url);
    next();
}

app.use(logRequest);

function logError(err, req, res, next) {
    log.error(err);
    next();
}

app.use(logError);

var port = argv.port;
var key = argv.key;
var ddRumAppId = argv.ddRumAppId;
var ddRumClientToken = argv.ddRumClientToken;

var keyEnabled = key !== null;
var ddRumEnabled = ddRumClientToken !== null && ddRumAppId !== null;

function shuffle(array) {
    if (array === undefined) {
        log.info("shuffle: array is undefined");
        return array;
    }

    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function endsWithAny(suffixes, string) {
    return suffixes.some(function (suffix) {
        return string.endsWith(suffix);
    });
}

function keyCheck(req, res, next) {
    if (key != null && (!req.query.hasOwnProperty('k') || req.query.k != key)) {
        log.info("Access denied!");
        res.status(403);
        res.send();
        return;
    }

    log.info("Access granted.");
    next();

}

function handleRoot(req, res) {
    if (!req.query.hasOwnProperty('a')) {

        var albumPaths = [];
        var yearDirs = fs.readdirSync('albums', {withFileTypes: true});
        for (const yearDir of yearDirs) {
            log.info(typeof yearDir);
            log.info(yearDir);
            if (yearDir.isDirectory()) {
                yearDirPath = 'albums/' + yearDir.name;
                var albumDirs = fs.readdirSync(yearDirPath, {withFileTypes: true});
                for (const albumDir of albumDirs) {
                    if (albumDir.isDirectory()) {
                        albumPaths.push(yearDir.name + '/' + albumDir.name);
                    }
                }
            }
        }

        var albumPathInfo = {
            albumPaths: albumPaths,
            keyEnabled: keyEnabled,
            key: key,
            rumEnabled: ddRumEnabled,
            clientToken: ddRumClientToken,
            applicationId: ddRumAppId
        }

        fs.readFile('albums.html', function(err, data) {
            var output = mustache.render(data.toString(), albumPathInfo);
            res.send(output);
        });
        return;
    }

    subdir = req.query.a;

    albumPath = 'albums/' + subdir;

    try {
        stats = fs.statSync(albumPath);
    } catch(e) {
        log.info(e);
        res.status(404);
        res.send();
        return;
    }

    fs.readdir(albumPath, function(err, items) {
        if (!req.query.hasOwnProperty('r')) {
            shuffle(items);
        }
        var fileNames = {
            names: [],
            keyEnabled: keyEnabled,
            key: key,
            rumEnabled: ddRumEnabled,
            clientToken: ddRumClientToken,
            applicationId: ddRumAppId
        }

        if (items.length == 0) {
            res.send("No images found :(")
            return;
        }

        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            var lowerItem = item.toLowerCase();
            suffixes = [
                '.jpg',
                '.gif',
                '.png',
            ]
            if (endsWithAny(suffixes, lowerItem)) {
                fileNames.names.push({'name': albumPath + '/' + item});
            }
        }

        fs.readFile('index.html', function(err, data) {
            var output = mustache.render(data.toString(), fileNames);
            res.send(output);
        });
    });
}

app.get('/', keyCheck, handleRoot);

app.use('/albums', keyCheck, express.static('albums'));
app.use('/js', express.static('js'));
app.use('/css', express.static('css'));

const server = app.listen(port, function() {
    log.info(`Photo Frame listening at http://localhost:${port}`);
    log.info("Key enabled: " + keyEnabled.toString());
    log.info("Datadog rum enabled: " + ddRumEnabled.toString());
});

process.on('SIGTERM', () => {
    console.info('SIGTERM signal received.');
    log.info('Closing http server.');
    server.close(() => {
        log.info('Http server closed.');
    });
});

$(document).ready(function() {
    const urlParams = new URLSearchParams(window.location.search);
    let slideDelayParam = urlParams.get('d');
    let slideDelay = parseInt(slideDelayParam, 10);
    if (isNaN(slideDelay)) {
        slideDelay = 30000;
    }

    $('.frame').slick({
        'autoplay': true,
        'autoplaySpeed': slideDelay,
        'useCSS': true,
        'arrows': false,
        'adaptiveHeight': true,
        'lazyLoad': 'progressive',
    });
    // TODO: Make an API call instead to pick up new image list and update
    // for now, reload once per hour to pick up new images
    window.setInterval(location.reload, 1000*60*60);
});
